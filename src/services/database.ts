// Remove this ignore once https://github.com/denoland/deno_lint/issues/1081 is resolved.
// deno-lint-ignore-file no-self-assign
/// <reference lib='dom' />
import { createContext } from '../deps.ts'

export interface GeolocationPositionWithName extends GeolocationPosition {
    name?: string
}

const GEOLOCATIONS_KEY = 'Ranger - Geolocation Positions'
const ACCURACY_KEY = 'Ranger - Minimum GPS Accuracy'
const BASE_KEY = 'Ranger - Base'
const DEFAULT_ACCURACY = 10
export const DEFAULT_BASE = -1

export class Database {
    #accuracy!: number
    #minimumAllowedAccuracyChanged = new Event('rangerDatabaseMinimumAllowedAccuracyChanged', { bubbles: true, composed: true, cancelable: true })

    #geolocationPositions!: Array<GeolocationPositionWithName>
    #geolocationPositionsChanged = new Event('rangerDatabaseGeolocationPositionsChanged', { bubbles: true, composed: true, cancelable: true })

    #base!: number
    #baseChanged = new Event('rangerDatabaseBaseChanged', { bubbles: true, composed: true, cancelable: true })

    constructor() {
        this.#geolocationPositions = this.geolocationPositions
        this.#accuracy = this.#accuracy
        this.#base = this.base
    }

    //#region ACCURACY
    get accuracy(): number {
        return +(this.#accuracy ?? localStorage.getItem(ACCURACY_KEY) ?? DEFAULT_ACCURACY)
    }

    public set accuracy(a: number) {
        localStorage.setItem(ACCURACY_KEY, a.toString())
        this.#accuracy = a
        dispatchEvent(this.#minimumAllowedAccuracyChanged)
    }
    //#endregion ACCURACY

    //#region POSITIONS
    /** TODO EXPLAIN THIS IS A COPY/NEW ARRAY */
    get geolocationPositions(): Array<GeolocationPositionWithName> {
        return [...(this.#geolocationPositions ?? JSON.parse(localStorage.getItem(GEOLOCATIONS_KEY) ?? '[]'))]
    }

    addPosition(p: GeolocationPositionWithName) {
        // TODO ensure unique timestamp

        const newItem: GeolocationPositionWithName = {  // JSON.stringify does not work on GeolocaitonPosition objects
            name: p.name,
            timestamp: p.timestamp,
            coords: {
                accuracy: p.coords.accuracy,
                altitude: p.coords.altitude,
                altitudeAccuracy: p.coords.altitudeAccuracy,
                heading: p.coords.heading,
                latitude: p.coords.latitude,
                longitude: p.coords.longitude,
                speed: p.coords.speed
            }
        }

        this.#geolocationPositions.push(newItem)
        localStorage.setItem(GEOLOCATIONS_KEY, JSON.stringify(this.#geolocationPositions));
        dispatchEvent(this.#geolocationPositionsChanged)
    }

    updatePositionName(timestamp:number, name:string) {
        const p = this.#geolocationPositions.find(p => p.timestamp === timestamp)
        if (p) {
            p.name = name
            localStorage.setItem(GEOLOCATIONS_KEY, JSON.stringify(this.#geolocationPositions));
            dispatchEvent(this.#geolocationPositionsChanged)
        }
    }

    removePosition(p: GeolocationPositionWithName) {
        this.#geolocationPositions.splice(this.#geolocationPositions.findIndex(pos => pos.timestamp === p.timestamp), 1)
        localStorage.setItem(GEOLOCATIONS_KEY, JSON.stringify(this.#geolocationPositions));
        dispatchEvent(this.#geolocationPositionsChanged)
        if (p.timestamp === this.#base) {
            this.base = DEFAULT_BASE
        }
    }

    removeAllPositions() {
        localStorage.setItem(GEOLOCATIONS_KEY, JSON.stringify([]));
        dispatchEvent(this.#geolocationPositionsChanged)
        this.#geolocationPositions = []
        this.base = DEFAULT_BASE
    }
    //#endregion POSITIONS

    //#region BASE
    /** The timestamp of the GeolocationPositionWithName which should be considered as the base. */
    get base(): number {
        return +(this.#base ?? localStorage.getItem(BASE_KEY) ?? DEFAULT_BASE)
    }

    public set base(timestamp: number) {
        localStorage.setItem(BASE_KEY, timestamp.toString())
        this.#base = timestamp
        dispatchEvent(this.#baseChanged)
    }

    removeBase() {
        this.base = DEFAULT_BASE
    }
    //#endregion BASE
}

export const databaseContext = createContext<Database>('database');

declare global {
    interface WindowEventMap {
        'rangerDatabaseGeolocationPositionsChanged': 'rangerDatabaseGeolocationPositionsChanged'
        'rangerDatabaseMinimumAllowedAccuracyChanged': 'rangerDatabaseMinimumAllowedAccuracyChanged'
        'rangerDatabaseBaseChanged': 'rangerDatabaseBaseChanged'
    }
}
