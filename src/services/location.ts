/// <reference lib='dom' />
import { createContext } from '../deps.ts'

// review https://www.w3.org/TR/geolocation/#geolocation_interface

/**
 * TODO explain each state here???
 */
export type GPS_STATE =
    'OFF' |
    'PENDING' |
    'CURRENT' |
    'TIMEOUT' |
    'UNAVAILABLE' |
    'DENIED'

export class Location {

    #watchID: number | null = null
    #state: GPS_STATE = 'OFF'
    #position: GeolocationPosition | undefined
    #lastKnownPosition: GeolocationPosition | undefined

    #rangerGPSOff = new CustomEvent('rangerGPSOff', { detail: '🚗', bubbles: true, composed: true, cancelable: true })
    #rangerGPSPending = new CustomEvent('rangerGPSPending', { detail: '🚕', bubbles: true, composed: true, cancelable: true })
    #rangerGPSTimeout = new CustomEvent('rangerGPSTimeout', { detail: '🚕', bubbles: true, composed: true, cancelable: true })
    #rangerGPSUnavailable = new CustomEvent('rangerGPSUnavailable', { detail: '🚕', bubbles: true, composed: true, cancelable: true })
    #rangerGPSDenied = new CustomEvent('rangerGPSDenied', { detail: '🚕', bubbles: true, composed: true, cancelable: true })

    // TODO Should i be using properties or methods to turn GPS off/on?

    #enabled = false

    /**
     * false in the following conditions:
     * 1. initially
     * 2. browser permission request for GPS is blocked/denied
     * 3. user expliclty turns off GPS (i.e., location.enabled = false)
     *
     * true otherwise
     */
    public get enabled(): boolean {
        return this.#enabled
    }

    /**
     * TODO explain
     */
    public set enabled(c: boolean) {
        this.#enabled = c;
        this.#handleToggle(c)
    }

    /**
     * The current state of the GPS
     * Will be one of:
     * - OFF
     * - PENDING
     * - CURRENT
     * - TIMEOUT
     * - UNAVAILABLE
     * - DENIED
    */
    public get state(): GPS_STATE {
        return this.#state
    }

    /**
     * The location, only if it is 'current', `undefined` otherwise.
     *
     * `position` is set to the `GeolocationPosition` returned by the `rangerGPSCurrent` event.
     * `position` is set to undefined for all other events.
     */
    public get position(): GeolocationPosition | undefined {
        return this.#position
    }

    /**
     * This is the location provided by the last `CURRENT` GPS event.
     *
     * `position` TODO
     * `position` TODO
     */
     public get lastKnownPosition(): GeolocationPosition | undefined {
        return this.#lastKnownPosition
    }

    #handleToggle(c: boolean) {
        if (c) {
            this.#state = 'PENDING'
            this.#position = undefined
            this.#watchID = navigator.geolocation.watchPosition(
                p => this.#successPositionCallback(p),
                e => this.#errorPositionCallback(e),
                { enableHighAccuracy: true, timeout: 1000 }
            )
            dispatchEvent(this.#rangerGPSPending)
        } else {
            this.#state = 'OFF'
            this.#position = undefined
            navigator.geolocation.clearWatch(this.#watchID!)
            dispatchEvent(this.#rangerGPSOff)
        }
    }

    #successPositionCallback(p: GeolocationPosition) {
        this.#state = 'CURRENT'
        this.#position = p
        this.#lastKnownPosition = p
        dispatchEvent(new CustomEvent<GeolocationPosition>('rangerGPSCurrent', {
            detail: p,
            bubbles: true,
            composed: true,
            cancelable: true
        }))
    }

    #errorPositionCallback(e: GeolocationPositionError) {
        switch (e.code) {
            case e.PERMISSION_DENIED:
                this.#state = 'DENIED'
                this.#position = undefined
                this.#enabled = false
                dispatchEvent(this.#rangerGPSDenied)
                break;
            case e.POSITION_UNAVAILABLE:
                this.#state = 'UNAVAILABLE'
                this.#position = undefined
                dispatchEvent(this.#rangerGPSUnavailable)
                break;
            case e.TIMEOUT:
                this.#state = 'TIMEOUT'
                this.#position = undefined
                dispatchEvent(this.#rangerGPSTimeout)
                break
        }
    }

}

export const locationContext = createContext<Location>('location');

declare global {
    interface WindowEventMap {
        'rangerGPSCurrent': CustomEvent<GeolocationPosition>
        'rangerGPSOff': string
        'rangerGPSPending': string
        'rangerGPSTimeout': string
        'rangerGPSUnavailable': string
        'rangerGPSDenied': string
    }
}
