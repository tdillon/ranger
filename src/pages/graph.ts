import { LitElement, html, customElement, css } from '../deps.ts'
import '../components/coordGraph.ts'
import '../components/addCoordButton.ts'

@customElement('graph-page')
export class GraphPage extends LitElement {

    static styles = css`:host { display: block; }`

    render() {
        return html`
            <coord-graph></coord-graph>
            <add-coord-button></add-coord-button>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'graph-page': GraphPage
    }
}
