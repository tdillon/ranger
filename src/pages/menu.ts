import { LitElement, html, customElement, css } from '../deps.ts'
import json from '../version.json' assert { type: 'json' }

@customElement('menu-page')
export class MenuPage extends LitElement {

    static styles = css`
        :host { display: block; }
        a { color: var(--link-color, chartreuse); }
        ul { padding: 0; margin: 0; list-style: none;}
        li { padding: .25em;}
    `

    render() {
        return html`
            <ul>
                <li><a href="/ranger/">Graph</a></li>
                <li><a href="/ranger/list">List</a></li>
                <li><a href="/ranger/admin">Admin</a></li>
                <li><a href="/ranger/developer">Developer</a></li>
            </ul>
            <hr>
            <ul>
                <li>Version: ${json.version}</li>
                <li>Build: <a href=https://gitlab.com/tdillon/ranger/-/pipelines/${json.pipeline}>${json.build}</a></li>
                <li>Commit: <a href=https://gitlab.com/tdillon/ranger/-/commit/${json.commit}>${json.commit.substring(0, 8)}</a></li>
            </ul>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'menu-page': MenuPage
    }
}
