import { LitElement, html, customElement, css } from '../deps.ts'
import '../components/setTestData.ts'
import '../components/translateData.ts'

@customElement('developer-page')
export class DeveloperPage extends LitElement {

    static styles = css`:host { display: block; }`

    render() {
        return html`
            <set-test-data></set-test-data>
            <hr>
            <translate-data></translate-data>
            <hr>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'developer-page': DeveloperPage
    }
}
