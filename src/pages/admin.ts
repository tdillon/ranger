import { LitElement, html, customElement, css } from '../deps.ts'
import '../components/gpsToggler.ts'
import '../components/gpsStatus.ts'
import '../components/gpsCoords.ts'
import '../components/gpsAccuracy.ts'
import '../components/setAccuracy.ts'

@customElement('admin-page')
export class AdminPage extends LitElement {

    static styles = css`:host { display: block; }`

    render() {
        return html`
            <my-gps-toggler></my-gps-toggler>
            <hr>
            <set-accuracy></set-accuracy>
            <hr>
            <my-gps-status></my-gps-status>
            <my-gps-coords></my-gps-coords>
            <my-gps-accuracy></my-gps-accuracy>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'admin-page': AdminPage
    }
}
