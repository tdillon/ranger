import { LitElement, html, customElement, css } from '../deps.ts'
import '../components/coordList.ts'
import '../components/addCoordButton.ts'

@customElement('list-page')
export class ListPage extends LitElement {

    static styles = css`:host { display: block; }`

    render() {
        return html`
            <coord-list></coord-list>
            <add-coord-button></add-coord-button>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'list-page': ListPage
    }
}
