import { customElement, html, LitElement, contextProvided, property, state, query } from '../deps.ts'
import { Database, databaseContext, GeolocationPositionWithName } from '../services/database.ts'

@customElement('edit-page')
export class EditPage extends LitElement {

    @property()
    timestamp!: number

    @contextProvided({ context: databaseContext, subscribe: true })
    database!: Database

    // @ts-ignore https://github.com/lit/lit/issues/3241
    @query('input[type="checkbox"]', true)
    cbBase!: HTMLInputElement

    // @ts-ignore https://github.com/lit/lit/issues/3241
    @query('input[type="text"]', true)
    txtName!: HTMLInputElement

    #gp?: GeolocationPositionWithName

    @state()
    isBase!: boolean

    render() {
        return html`
            <div>Coordinate with timestamp: ${this.timestamp}</div>
            <div><input type=text placeholder="name of position" value=${this.#gp?.name}></div>
            <div>
                <label>
                    <span>Base</span>
                    <input type=checkbox .checked=${this.isBase}>
                </label>
            </div>
            <div>
                <button @click=${this.#save}>Save</button>
                <button @click=${this.#goBack}>Cancel</button>
                <button @click=${this.#delete}>Delete</button>
            </div>
        `
    }

    #save() {
        if (this.isBase && !this.cbBase.checked) {
            this.database.removeBase()
        } else if (!this.isBase && this.cbBase.checked) {
            this.database.base = this.timestamp
        }

        if (this.#gp!.name !== this.txtName.value) {
            this.#gp!.name = this.txtName.value
            this.database.updatePositionName(this.#gp!.timestamp, this.#gp!.name)
        }

        this.#goBack()
    }

    #goBack() {
        window.history.back()
    }

    #delete() {
        this.database.removePosition(this.#gp!)
        this.#goBack()
    }

    protected override firstUpdated() {
        const gp = this.database.geolocationPositions.find(p => p.timestamp === this.timestamp)
        this.isBase = this.database.base === this.timestamp

        if (gp) {
            this.#gp = gp
        } else {
            console.error(`[CoordEdit] ${this.timestamp} should be in the database.`)
            // TODO should i `this.#goBack()`?
        }
    }

}

declare global {
    interface HTMLElementTagNameMap {
        'edit-page': EditPage
    }
}
