import { html, customElement, css } from '../deps.ts'
import { GPSLitElement } from './gpsLitElement.ts';

@customElement('my-gps-status')
export class MyGPSStatus extends GPSLitElement {

    static styles = css`
        :host{
            display:block;
            text-align:center;
            width:fit-content;
        }
        div {
            padding:8px;
        }
        span:first-child{
            font-weight:bold;
        }
        span:last-child{
            font-size:.8em;
        }
        .OFF{background:#423f3f;}
        .PENDING{background:#83984d;}
        .CURRENT{background:#336b8b;}
        .DENIED{background:#f0b51f;}
        .TIMEOUT{background:#ea700b;}
        .UNAVAILABLE{background:red;}
    `

    render() {
        return html`
            <div class=${this.gpsState}>
                <span>GPS status</span>
                <hr>
                <span>${this.gpsState}</span>
            </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'my-gps-status': MyGPSStatus;
    }
}
