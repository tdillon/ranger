import { customElement, html, css } from '../deps.ts';
import { GPSLitElement } from './gpsLitElement.ts';

@customElement('my-gps-accuracy')
export class MyGPSAccuracy extends GPSLitElement {

    static styles = css`
        :host{
            display:block;
            text-align:center;
            width:fit-content;
        }
        div {
            padding:8px;
        }
        span:first-child{
            font-weight:bold;
        }
        span:last-child{
            font-size:.8em;
        }
        .OFF,.PENDING,.DENIED,.UNAVAILABLE{background:#423f3f;}
        .CURRENT{background:#336b8b;}
        .TIMEOUT{background:#ea700b;}
    `

    render() {
        return html`
            <div class=${this.gpsState}>
                <span>GPS Accuracy</span>
                <hr>
                <span>${this.gpsCurrentPosition?.coords.accuracy || '🤪'}</span>
            </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'my-gps-accuracy': MyGPSAccuracy;
    }
}
