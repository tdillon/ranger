import { html, customElement } from '../deps.ts'
import { GPSLitElement } from './gpsLitElement.ts'

@customElement('my-gps-toggler')
export class MyGPSToggler extends GPSLitElement {

    #handleToggle() {
        this.location.enabled = (this.gpsState === 'OFF' || this.gpsState === 'DENIED')
    }

    render() {
        return html`
            <label title=foo>
                <input type=checkbox @input=${this.#handleToggle} .checked=${this.gpsState !== 'DENIED' && this.gpsState !== 'OFF'}>
                <span>${this.gpsState}</span>
            </label>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'my-gps-toggler': MyGPSToggler;
    }
}
