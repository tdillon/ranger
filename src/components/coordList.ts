import { state, customElement, html, contextProvided, css, nothing, CheapRuler, Point } from '../deps.ts'
import { Database, databaseContext, GeolocationPositionWithName } from '../services/database.ts'
import { GPSLitElement } from "./gpsLitElement.ts";

@customElement('coord-list')
export class CoordList extends GPSLitElement {

    static styles = css`
        :host { display:block; }
        a {
            text-decoration: none;
            color: white;
            display: block;
            padding: 8px 0;
            position:relative;
        }
        span{
            font-size:.75em;
            color:#fff6;
            display:block;
        }
        span:first-child{
            font-size: inherit;
            color:inherit;
        }
        .base {
            color:inherit;
            position:absolute;
            right:0;
            top: calc(8px - .25em);
            font-size:1.5em;
            line-height:1em;
        }
    `

    @contextProvided({ context: databaseContext, subscribe: true })
    database!: Database;

    @state()
    geolocationPositions?: Array<GeolocationPositionWithName>

    @state()
    base!: number

    render() {
        let rulerCurrent: CheapRuler, rulerBase: CheapRuler
        let p: Point, bp: Point
        const bgp = this.geolocationPositions?.find(gp => gp.timestamp === this.base)

        if (this.gpsCurrentPosition) {
            p = [this.gpsCurrentPosition.coords.longitude, this.gpsCurrentPosition.coords.latitude]
            rulerCurrent = new CheapRuler(this.gpsCurrentPosition?.coords.latitude, 'yards')
        }

        if (bgp) {
            bp = [bgp.coords.longitude, bgp.coords.latitude]
            rulerBase = new CheapRuler(bp[1], 'yards')
        }

        return this.geolocationPositions?.map(gp => html`
            <a href=${gp.timestamp}>
                <span>${gp.name ? gp.name : '[no name]'}</span>${(this.base === gp.timestamp) ? html`
                <span class=base>⭐</span>` : nothing} ${this.gpsCurrentPosition && rulerCurrent ? html`
                <span>${rulerCurrent.distance(p, [gp.coords.longitude, gp.coords.latitude]).toFixed(0)} yards to current</span>` : nothing}
                ${gp.timestamp !== bgp?.timestamp && bp ? html`<span>${rulerBase.distance(bp, [gp.coords.longitude, gp.coords.latitude]).toFixed(0)} yards to <i>${bgp?.name ? bgp.name : '[no name]'}</i></span>` : nothing}
            </a>
        `)
    }

    #getGeolocationPositions = () => {
        this.geolocationPositions = this.database.geolocationPositions
    }

    #getBase = () => {
        this.base = this.database.base
    }

    connectedCallback() {
        super.connectedCallback()
        this.#getGeolocationPositions()
        this.#getBase()
        self.addEventListener('rangerDatabaseGeolocationPositionsChanged', this.#getGeolocationPositions)
        self.addEventListener('rangerDatabaseBaseChanged', this.#getBase)
    }

    disconnectedCallback() {
        self.removeEventListener('rangerDatabaseGeolocationPositionsChanged', this.#getGeolocationPositions)
        self.removeEventListener('rangerDatabaseBaseChanged', this.#getBase)
        super.disconnectedCallback()
    }

}

declare global {
    interface HTMLElementTagNameMap {
        'coord-list': CoordList;
    }
}
