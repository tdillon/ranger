import { customElement, html, contextProvided, state, css, CheapRuler, Point } from '../deps.ts'
import { GPSLitElement } from './gpsLitElement.ts'
import { Database, databaseContext } from '../services/database.ts'

@customElement('translate-data')
export class TranslateData extends GPSLitElement {

    static styles = css`
        button {
            padding: .5em;
            line-height: 1em;
            background: var(--button-active-background, chartreuse);
            border: solid 1px var(--button-active-border, chartreuse);
            color: var(--button-active-color, chartreuse);
        }
        button:disabled {
            background: var(--button-inactive-background, chartreuse);
            border: solid 1px var(--button-inactive-border, chartreuse);
            color: var(--button-inactive-color, chartreuse);
            cursor: not-allowed;
        }
    `

    /** False if 0 positions are saved in the database.  True otherwise. */
    @state()
    hasPositions = false

    @contextProvided({ context: databaseContext, subscribe: true })
    database!: Database;

    render() {
        return html`
            <p>
                Click the button below to translate the current position data to the current GPS location.
                GPS must be turned on for this option to be available.
                There must be at least 1 position saved for this option to be available.
                The average longitude/latitude of saved positions is translated to the current GPS location.
            </p>
            <button ?disabled=${!(this.hasPositions && this.gpsState === 'CURRENT')} @click=${this.handleClick}>Translate Data</button>
        `
    }

    private handleClick() {
        if (!this.location.position) {
            console.error('[translate-data] location.position should have been set')
            return
        }

        const positions = this.database.geolocationPositions
        if (positions.length === 0) {
            console.error('[translate-data] there must be GPS coordinates in the database')
            return
        }

        const currentGPSPos: [longitude: number, latitude: number] = [this.location.position.coords.longitude, this.location.position.coords.latitude]
        const rulerOrig = new CheapRuler(positions[0].coords.latitude, 'yards')
        const rulerNew = new CheapRuler(currentGPSPos[1], 'yards')
        // TODO currently using average lon/lat because it was easy to calc.  Does it matter?  Should we use center instead?
        const center: Point = [positions.reduce((p, c) => p + c.coords.longitude, 0) / positions.length, positions.reduce((p, c) => p + c.coords.latitude, 0) / positions.length]

        this.database.removeAllPositions()

        positions.forEach(p => {
            const d = rulerOrig.distance(center, [p.coords.longitude, p.coords.latitude])
            const b = rulerOrig.bearing(center, [p.coords.longitude, p.coords.latitude])
            const newPos = rulerNew.destination(currentGPSPos, d, b)

            this.database.addPosition({
                name: p.name,
                timestamp: p.timestamp,
                coords: {
                    accuracy: p.coords.accuracy,
                    altitude: p.coords.altitude,
                    altitudeAccuracy: p.coords.altitudeAccuracy,
                    heading: p.coords.heading,
                    speed: p.coords.speed,
                    longitude: newPos[0],
                    latitude: newPos[1]
                }
            })
        })
    }

    private getGeolocationPositions = () => {
        this.hasPositions = this.database.geolocationPositions.length > 0
    }

    override connectedCallback() {
        super.connectedCallback()
        this.getGeolocationPositions()
        self.addEventListener('rangerDatabaseGeolocationPositionsChanged', this.getGeolocationPositions)
    }

    override disconnectedCallback() {
        self.removeEventListener('rangerDatabaseGeolocationPositionsChanged', this.getGeolocationPositions)
        super.disconnectedCallback()
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'translate-data': TranslateData;
    }
}
