import { customElement, html, contextProvided, css, query, } from '../deps.ts';
import { GPSLitElement } from './gpsLitElement.ts';
import { Database, databaseContext } from '../services/database.ts'

@customElement('add-coord-button')
export class AddCoordButton extends GPSLitElement {
    static styles = css`
        button {
            padding: .5em;
            line-height: 1em;
            font-size: 1em;
            background: var(--button-active-background, chartreuse);
            border: solid 1px var(--button-active-border, chartreuse);
            color: var(--button-active-color, chartreuse);
        }
        button:disabled {
            background: var(--button-inactive-background, chartreuse);
            border: solid 1px var(--button-inactive-border, chartreuse);
            color: var(--button-inactive-color, chartreuse);
            cursor: not-allowed;
        }
        a[hidden] { display: none; } /** TODO Why doesn't hidden attribute work without this? */
    `

    // @ts-ignore https://github.com/lit/lit/issues/3241
    @query('a', true)
    lnk!: HTMLAnchorElement

    @contextProvided({ context: databaseContext, subscribe: true })
    database!: Database;

    #handleClick() {
        if (this.gpsCurrentPosition) {
            this.database.addPosition(this.gpsCurrentPosition)
            this.lnk.click()
        } else {
            console.error('[AddCoordButton] How did this happen?')
        }
    }

    render() {
        return html`
            <button ?disabled=${this.gpsState !== 'CURRENT'} @click=${this.#handleClick}>➕</button>
            <a hidden href=${this.gpsCurrentPosition?.timestamp}>➕</a>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'add-coord-button': AddCoordButton;
    }
}
