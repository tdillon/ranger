/// <reference lib='dom' />
import { contextProvided, LitElement, state } from '../deps.ts'
import { locationContext, Location, GPS_STATE } from '../services/location.ts'

export abstract class GPSLitElement extends LitElement {

  // TODO should this be private/protected/public?
  @contextProvided({ context: locationContext, subscribe: true })
  location!: Location;

  /** The current state of the GPS */
  @state()
  protected gpsState!: GPS_STATE

  /**
   * The location, only if it is 'current', `undefined` otherwise.
   *
   * `currentPosition` is set to the `GeolocationPosition` returned by the `rangerGPSCurrent` event.
   * `currentPosition` is set to undefined for all other events.
   */
  @state()
  protected gpsCurrentPosition?: GeolocationPosition

  /**
   * // TODO should we set this value from this.location?
   * // TOOD should subclasses be using this.location?
   * The last received location, even if it is stale or GPS is off.
   *
   * `lastPosition` is only set during the `rangerGPSCurrent` event
   */
  // @state() protected lastPosition?: GeolocationPosition

  // onRanger* methods will/can be overriden to provide desired behavior

  /**
   * @example
   * override onRangerGPSCurrent(p: CustomEvent<GeolocationPosition>) {
   *   super.onRangerGPSCurrent(p)
   *   // do stuff
   * }
   */
  protected onRangerGPSCurrent(p: CustomEvent<GeolocationPosition>) {
    this.gpsState = 'CURRENT'
    this.gpsCurrentPosition = p.detail
  }

  /**
   * @example
   * override onRangerGPSDenied() {
   *   super.onRangerGPSDenied()
   *   // do stuff
   * }
   */
  protected onRangerGPSDenied() {
    this.gpsState = 'DENIED'
    this.gpsCurrentPosition = undefined
  }

  /**
   * @example
   * override onRangerGPSOff() {
   *   super.onRangerGPSOff()
   *   // do stuff
   * }
   */
  protected onRangerGPSOff() {
    this.gpsState = 'OFF'
    this.gpsCurrentPosition = undefined
  }

  /**
   * @example
   * override onRangerGPSPending() {
   *   super.onRangerGPSPending()
   *   // do stuff
   * }
   */
  protected onRangerGPSPending() {
    this.gpsState = 'PENDING'
    this.gpsCurrentPosition = undefined
  }

  /**
   * @example
   * override onRangerGPSTimeout() {
   *   super.onRangerGPSTimeout()
   *   // do stuff
   * }
   */
  protected onRangerGPSTimeout() {
    this.gpsState = 'TIMEOUT'
    this.gpsCurrentPosition = undefined
  }

  /**
   * @example
   * override onRangerGPSUnavailable() {
   *   super.onRangerGPSUnavailable()
   *   // do stuff
   * }
   */
  protected onRangerGPSUnavailable() {
    this.gpsState = 'UNAVAILABLE'
    this.gpsCurrentPosition = undefined
  }

  // listener* properties are for proper event listener cleanup
  private listenerCurrent = (p: CustomEvent<GeolocationPosition>) => { this.onRangerGPSCurrent(p) }
  private listenerDenied = () => { this.onRangerGPSDenied() }
  private listenerOff = () => { this.onRangerGPSOff() }
  private listenerPending = () => { this.onRangerGPSPending() }
  private listenerTimeout = () => { this.onRangerGPSTimeout() }
  private listenerUnavailable = () => { this.onRangerGPSUnavailable() }

  connectedCallback() {
    super.connectedCallback()

    this.gpsState = this.location.state ?? 'OFF'
    this.gpsCurrentPosition = this.location.position

    self.addEventListener('rangerGPSCurrent', this.listenerCurrent)
    self.addEventListener('rangerGPSDenied', this.listenerDenied)
    self.addEventListener('rangerGPSOff', this.listenerOff)
    self.addEventListener('rangerGPSPending', this.listenerPending)
    self.addEventListener('rangerGPSTimeout', this.listenerTimeout)
    self.addEventListener('rangerGPSUnavailable', this.listenerUnavailable)
  }

  disconnectedCallback() {
    self.removeEventListener('rangerGPSCurrent', this.listenerCurrent)
    self.removeEventListener('rangerGPSDenied', this.listenerDenied)
    self.removeEventListener('rangerGPSOff', this.listenerOff)
    self.removeEventListener('rangerGPSPending', this.listenerPending)
    self.removeEventListener('rangerGPSTimeout', this.listenerTimeout)
    self.removeEventListener('rangerGPSUnavailable', this.listenerUnavailable)

    super.disconnectedCallback()
  }
}
