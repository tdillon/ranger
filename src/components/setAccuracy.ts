import { customElement, html, contextProvided, css, LitElement, state } from '../deps.ts'
import { Database, databaseContext } from '../services/database.ts'

@customElement('set-accuracy')
export class SetAccuracy extends LitElement {

    static styles = css`
        h1,h2,div{margin:1em 0;}
        h1{font-size:1em;}
        h2{font-size:.8em;font-weight:normal;}
        div{margin:0}
        button {
            padding: .5em;
            line-height: 1em;
            background: var(--button-active-background, chartreuse);
            border: solid 1px var(--button-active-border, chartreuse);
            color: var(--button-active-color, chartreuse);
        }
        span { margin: 0 .5em;  }
    `

    @contextProvided({ context: databaseContext, subscribe: true })
    database!: Database;

    @state()
    private accuracy!: number

    private handleDecrease() {
        this.database.accuracy = this.accuracy - 1
    }

    private handleIncrease() {
        this.database.accuracy = this.accuracy + 1
    }

    render() {
        return html`
            <h1>Minimum Allowed Accuracy</h1>
            <h2>
                The number (in yards) which the GPS accuracy must be less than in order for the add GPS buttons to be enabled.
                To set this value properly, first determine the best accuracy your phone's GPS is capable of producing, and then set this value to the next highest integer.
            </h2>
            <div>
                <button @click=${this.handleDecrease}>◀</button>
                <span>${this.accuracy}</span>
                <button @click=${this.handleIncrease}>▶</button>
            </div>
        `
    }

    private getAccuracy = () => {
        this.accuracy = this.database.accuracy
    }

    override connectedCallback() {
        super.connectedCallback()
        this.getAccuracy()
        self.addEventListener('rangerDatabaseMinimumAllowedAccuracyChanged', this.getAccuracy)
    }

    override disconnectedCallback() {
        self.removeEventListener('rangerDatabaseMinimumAllowedAccuracyChanged', this.getAccuracy)
        super.disconnectedCallback()
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'set-accuracy': SetAccuracy;
    }
}
