/// <reference lib='dom' />
import { customElement, html, contextProvided, query, CheapRuler, Point, css } from '../deps.ts'
import { GPSLitElement } from './gpsLitElement.ts'
import { Database, databaseContext, GeolocationPositionWithName } from '../services/database.ts'

@customElement('coord-graph')
export class CoordGraph extends GPSLitElement {

    static styles = css`
        :host { display: block; white-space: nowrap;}
        canvas { width: 100%; }
    `;

    static DPR = window.devicePixelRatio

    @contextProvided({ context: databaseContext, subscribe: true })
    database!: Database;

    geolocationPositions?: Array<GeolocationPositionWithName>
    #base!: number;

    // @ts-ignore WHY!!!!
    @query('canvas', true) cvs: HTMLCanvasElement

    ctx!: CanvasRenderingContext2D

    render() {
        return html`<canvas></canvas>`
    }

    private draw() {
        /**
         * TODO - all combinations of GPS STATE, BASE STATE, CONDITIONS need to be handled
         *
         * GPS STATE:
         * 1. GPS on|current
         * 2. GPS on|bad
         * 3. GPS off
         *
         * BASE STATE:
         * 1. current GPS location is set as "base"
         * 2. a location from database is set as "base"
         * 3. no location is set as "base" (should this be allowed, or default to something?)
         *
         * CONDITIONS:
         * 1. no locations in database (i.e., this.geolocationPositions is null/undefined, or length === 0)
         * 2. N locations in the database, N > 0
         * 3. max distance between points/base/gps is big
         * 4. max distance between points/base/gps is small
         */

        this.ctx.clearRect(0, 0, this.cvs.width, this.cvs.height)

        /** x and y coordinate of the center of the canvas */
        const center = this.cvs.width / 2
        /** radius of the dots rendered in the canvas for each position  TODO maybe make it based on real world distance, e.g., 5ft */
        const positionDotRadius = 5
        const maxRadius = center - positionDotRadius * 2 - 1 // 1ish padding from most distant position

        if (this.geolocationPositions?.length) {
            // get "center" of positions TODO currently doing 'average'
            const centerOfPositions: Point = [this.geolocationPositions.reduce((p, c) => p + c.coords.longitude, 0) / this.geolocationPositions.length, this.geolocationPositions.reduce((p, c) => p + c.coords.latitude, 0) / this.geolocationPositions.length]
            const ruler = new CheapRuler(centerOfPositions[1], 'yards')
            const currentGPSpoint: Point | undefined = this.location.lastKnownPosition ? [this.location.lastKnownPosition.coords.longitude, this.location.lastKnownPosition.coords.latitude] : undefined

            // TODO base should be 'center', current GPS, or 'base' from database
            const base = centerOfPositions
            const maxDist = Math.max(this.geolocationPositions.reduce((prev, cur) => Math.max(ruler.distance(base, [cur.coords.longitude, cur.coords.latitude]), prev), 0), currentGPSpoint ? ruler.distance(base, currentGPSpoint) : 0)
            const pxPerYard = maxRadius / maxDist

            // Print rings on 25 yard increments < max distance.  100 yard rings are bold
            this.ctx.strokeStyle = 'green'
            for (let i = 25; i < maxDist; i += 25) {
                this.ctx.lineWidth = (i % 100 ? 1 : 2) * CoordGraph.DPR
                this.ctx.beginPath()
                this.ctx.arc(center, center, i * pxPerYard, 0, Math.PI * 2)
                this.ctx.stroke()
            }

            for (let i = 0; i < this.geolocationPositions.length; ++i) {
                this.ctx.fillStyle = this.#base === this.geolocationPositions[i].timestamp ? '#f00' : '#09c'
                /** ith position */
                const current: Point = [this.geolocationPositions[i].coords.longitude, this.geolocationPositions[i].coords.latitude];
                /** distance in yards from base to ith position */
                const distance = ruler.distance(base, current)
                /** angle from north (in radians) from base to ith position. east > 0, west < 0 */
                const bearing = ruler.bearing(base, current) * Math.PI / 180
                /** x position in canvas of ith position */
                const x = center + distance * Math.sin(bearing) * pxPerYard
                /** y position in canvas of ith position */
                const y = center - distance * Math.cos(bearing) * pxPerYard

                this.ctx.beginPath()
                this.ctx.arc(x, y, positionDotRadius, 0, 2 * Math.PI)
                this.ctx.fill()
            }

            // draw gps location based on status
            if (currentGPSpoint) {
                switch (this.gpsState) {  // match colors on gpsStatus.ts
                    case 'CURRENT': this.ctx.fillStyle = '#336b8b'; break;
                    case 'DENIED': this.ctx.fillStyle = '#f0b51f'; break;
                    case 'OFF': this.ctx.fillStyle = '#423f3f'; break;
                    case 'PENDING': this.ctx.fillStyle = '#83984d'; break;
                    case 'TIMEOUT': this.ctx.fillStyle = '#ea700b'; break;
                    case 'UNAVAILABLE': this.ctx.fillStyle = 'red'; break;
                }
                /** distance in yards from base to ith position */
                const distance = ruler.distance(base, currentGPSpoint)
                /** angle from north (in radians) from base to ith position. east > 0, west < 0 */
                const bearing = ruler.bearing(base, currentGPSpoint) * Math.PI / 180
                /** x position in canvas of ith position */
                const x = center + distance * Math.sin(bearing) * pxPerYard
                /** y position in canvas of ith position */
                const y = center - distance * Math.cos(bearing) * pxPerYard

                this.ctx.beginPath()
                this.ctx.arc(x, y, positionDotRadius * 2, 0, 2 * Math.PI)
                this.ctx.fill()
            }

        } else {
            // TODO what do i draw when no positions are saved?
            this.ctx.fillStyle = '#09c'
            this.ctx.fillRect(5, 5, Math.random() * 100, Math.random() * 100)
        }
    }

    private getGeolocationPositions = () => {
        this.geolocationPositions = this.database.geolocationPositions
        this.draw()
    }

    #getBase = () => {
        this.#base = this.database.base
        this.draw()
    }

    override updated() {
        // Using this method to ensure the canvas element exists.
        this.ctx = this.cvs.getContext('2d') as CanvasRenderingContext2D
        this.cvs.width = this.cvs.height = this.cvs.clientWidth * CoordGraph.DPR
        this.cvs.style.width = this.cvs.style.height = `${this.cvs.clientWidth}px`

        this.getGeolocationPositions()
        this.#getBase()
    }

    connectedCallback() {
        super.connectedCallback()
        self.addEventListener('rangerDatabaseGeolocationPositionsChanged', this.getGeolocationPositions)
        self.addEventListener('rangerDatabaseBaseChanged', this.#getBase)
    }

    disconnectedCallback() {
        self.removeEventListener('rangerDatabaseGeolocationPositionsChanged', this.getGeolocationPositions)
        self.removeEventListener('rangerDatabaseBaseChanged', this.#getBase)
        super.disconnectedCallback()
    }

}

declare global {
    interface HTMLElementTagNameMap {
        'coord-graph': CoordGraph;
    }
}
