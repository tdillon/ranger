import { customElement, html, contextProvided, css, LitElement } from '../deps.ts';
import { Database, databaseContext } from '../services/database.ts'

@customElement('set-test-data')
export class SetTestData extends LitElement {

    static readonly OHIO_STADIUM_TEST_DATA: ReadonlyArray<[longitude: number, latitude: number, name: string]> = [
        [-83.01996141672134, 40.00208737042245, 'NW end zone'],
        [-83.01999293267725, 40.00188704384994, 'NW 25 yard line'],
        [-83.02002511918545, 40.00168337789869, 'W 50 yard line'],
        [-83.02005797624588, 40.00147585887712, 'SW 25 yard line'],
        [-83.02008882164955, 40.00127450318401, 'SW end zone'],
        [-83.01974013447762, 40.001655897074414, 'center of field'],
        [-83.01966033875942, 40.00216082335174, 'N goalpost'],
        [-83.01982160657644, 40.00114916923843, 'S goalpost'],
        [-83.01856398582458, 40.00331705450523, 'parking lot'],
    ]

    static styles = css`
        button {
            padding: .5em;
            line-height: 1em;
            background: var(--button-active-background, chartreuse);
            border: solid 1px var(--button-active-border, chartreuse);
            color: var(--button-active-color, chartreuse);
        }
    `

    @contextProvided({ context: databaseContext, subscribe: true })
    database!: Database;

    private handleClick() {
        this.database.removeAllPositions()

        SetTestData.OHIO_STADIUM_TEST_DATA.forEach((ll, i) => this.database.addPosition({
            name: ll[2],
            timestamp: Date.now() + i,
            coords: {
                accuracy: 0,
                altitude: null,
                altitudeAccuracy: null,
                heading: null,
                speed: null,
                longitude: ll[0],
                latitude: ll[1]
            }
        }))
    }

    render() {
        return html`
            <p>Click the button below to remove all existing position data and add testing data.</p>
            <button @click=${this.handleClick}>Set Test Data</button>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'set-test-data': SetTestData;
    }
}
