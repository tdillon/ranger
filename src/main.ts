import { App } from './app.ts'
import json from "./version.json" assert { type: "json" };
import { URLPattern as BOB } from './deps.ts'

// https://developer.mozilla.org/en-US/docs/Web/API/URLPattern
// Once Firefox implments UrlPatten, remove this:
// @ts-ignore: Property 'UrlPattern' does not exist
if (!globalThis.URLPattern) {
  // @ts-ignore: Property 'UrlPattern' does not exist
  globalThis.URLPattern = BOB
}

if ("serviceWorker" in navigator) {
  await navigator.serviceWorker.register("sw.js", { type: "module" });
}

document.title = `Ranger - ${json.version}`
document.body.appendChild(new App())
