/// <reference lib='webworker' />
import json from "./version.json" assert { type: "json" };

// Read and understand the following:
// https://developer.chrome.com/docs/workbox/service-worker-lifecycle/

const sw = self as unknown as ServiceWorkerGlobalScope

const cacheName = `ranger-${json.version}`

const contentToCache = [
    '/ranger/',
    '/ranger/icon-192x192.png',
    '/ranger/icon-512x512.png',
    '/ranger/icon.svg',
    '/ranger/index.html',
    '/ranger/main.css',
    '/ranger/main.js',
    '/ranger/manifest.webmanifest'
]

sw.addEventListener('install', e => {
    e.waitUntil((async () => {
        const cache = await caches.open(cacheName);
        return cache.addAll(contentToCache);
    })())
})

sw.addEventListener('activate', e => {
    e.waitUntil((async () => {
        for (const key of await caches.keys()) {
            if (key !== cacheName) {
                await caches.delete(key)
            }
        }
    })())
})

sw.addEventListener('fetch', e => {
    e.respondWith((async () => {
        const cache = await caches.open(cacheName)
        const cachedResponse = await cache.match(e.request, { ignoreVary: true })
        return cachedResponse || await fetch(e.request)
    })())
})
