import { html, LitElement, css, customElement, contextProvider, Router } from './deps.ts'
import { Database, databaseContext, DEFAULT_BASE } from './services/database.ts'
import { Location, locationContext } from './services/location.ts'

import './pages/menu.ts'
import './pages/admin.ts'
import './pages/list.ts'
import './pages/graph.ts'
import './pages/developer.ts'
import './pages/edit.ts'

@customElement('my-app')
export class App extends LitElement {

  @contextProvider({ context: databaseContext })
  database = new Database()

  @contextProvider({ context: locationContext })
  location = new Location()

  static styles = css`
    :host {
      display: block;
    }
    main {
      padding: 8px;
    }
    a, button {
      background: none;
      border: none;
      color: #fff;
      cursor: pointer;
      float: right;
      font-family: monospace;
      font-size: 1.5em;
      font-weight: bold;
      line-height: 1em;
      padding: .5em;
      position: relative;
      text-decoration: none;
      z-index: 1;
    }
  `

  // TODO mobile isn't loading any route by default. look into that.

  private _router = new Router(this, [
    { path: '/ranger/menu', render: () => html`<menu-page></menu-page>` },
    { path: '/ranger/admin', render: () => html`<admin-page></admin-page>` },
    { path: '/ranger/list', render: () => html`<list-page></list-page>` },
    { path: '/ranger/developer', render: () => html`<developer-page></developer-page>` },
    { path: '/ranger/', render: () => html`<graph-page></graph-page>` },
    { path: '/ranger/index.html', render: () => html`<graph-page></graph-page>` },
    // TODO it feels gross that `timestamp` is a `string|undefined` but it can still be assigned to the `timestamp` property without forcing to a `number`.
    { path: '/ranger/:timestamp', render: ({ timestamp }) => html`<edit-page .timestamp=${+(timestamp ?? DEFAULT_BASE)}></edit-page>` },
  ])

  render() {
    return html`
      ${location.pathname === '/ranger/menu' ?
        html`<button @click=${this.#goBack}>🞨</button>` :
        html`<a href=${this._router.link('/ranger/menu')}>☰</a>`
      }
      <main>${this._router.outlet()}</main>
    `
  }

  #goBack = () => {
    window.history.back()
  }

}

declare global {
  interface HTMLElementTagNameMap {
    'my-app': App
  }
}
