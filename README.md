# Ranger

TODO

## Development

TODO

### Local dev

- Use `Live Preview: Start Server` option in VS Code.
- Navigate to the `ranger/` directory.
- Run `deno task build:local` to update the app.

### Git management

- Begin work on new thing?
- `git checkout master`
- `git pull`
- `git checkout -b feature/<what-you-are-working-on>`
- commit changes
- push changes
- create merge request (MR) in GitLab
- merge the MR
- `git checkout master`
- `git pull`
- `git fetch --prune`
- `git branch -D feature<tab>`

### Creating releases

When you want to release, update the version in `src/version.json` on your feature branch.  When the master branch builds, it will deploy the new app version.

# GPS Location Widget States/Events

```mermaid
stateDiagram-v2
    Off
    Pending
    Current
    Timeout
    Denied
    Unavailable

    [*] --> Off : by default, GPS is off
    Off --> Pending : 'GPS on' selected

    Pending --> Denied : GeolocationPositionError - code 1
    Denied --> Pending : 'GPS on' selected

    Timeout --> Current : GeolocationPosition
    Pending --> Current : GeolocationPosition
    Unavailable --> Current : GeolocationPosition

    Pending --> Timeout : GeolocationPositionError - code 3
    Current --> Timeout : GeolocationPositionError - code 3
    Unavailable --> Timeout : GeolocationPositionError - code 3

    Pending --> Unavailable : GeolocationPositionError - code 2
    Timeout --> Unavailable : GeolocationPositionError - code 2
    Current --> Unavailable : GeolocationPositionError - code 2

    Pending --> Off : 'GPS off' selected
    Unavailable --> Off : 'GPS off' selected
    Timeout --> Off : 'GPS off' selected
    Current --> Off : 'GPS off' selected
```
